import 'package:flutter/cupertino.dart';

// 封装获取屏幕高宽
class ScreenConstants {
  final BuildContext context;

  ScreenConstants(this.context);

  // 宽度
  double get screenWidth => MediaQuery.of(context).size.width;
  // 高度
  double get screenHeight => MediaQuery.of(context).size.height;
}
