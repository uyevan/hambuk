// ignore_for_file: constant_identifier_names, non_constant_identifier_names

import 'package:flutter/material.dart';

// 颜色配置
class ColorConstants {
  static const Color primary_red = Color(0xFFF2264B);
  static const Color primary_black = Color(0xFF000000);
  static const Color primary_dark = Color(0xFF313140);
  static const Color primary_grey = Color(0xFFB7B7C8);
  static const Color primary_sliver = Color(0xFFF8F8F8);
  static const Color primary_white = Color(0xFFFFFFFF);

  // 逐变色
  Color gradient_blue =
      ColorTween(begin: const Color(0xFF005BEA), end: const Color(0xFF00C6FB))
          as Color;
  Color? gradient_red =
      ColorTween(begin: const Color(0xFFED1D24), end: const Color(0xFFED1F69))
          .lerp(0.5);
  Color gradient_purple =
      ColorTween(begin: const Color(0xFFB224EF), end: const Color(0xFF7579FF))
          as Color;
  Color gradient_green =
      ColorTween(begin: const Color(0xFF0BA360), end: const Color(0xFF3CBA92))
          as Color;
  Color gradient_pink =
      ColorTween(begin: const Color(0xFFFF7EB3), end: const Color(0xFFFF758C))
          as Color;
  Color gradient_black_but =
      ColorTween(begin: const Color(0x00000000), end: const Color(0xFF000000))
          as Color;
  Color gradient_black_top =
      ColorTween(begin: const Color(0xFF000000), end: const Color(0x00000000))
          as Color;

  static Color translucentColor(Color color, double opacity) {
    int alpha = (opacity * 255).toInt();
    return color.withAlpha(alpha);
  }
}
