import 'package:hambuk/config/apiConfig.dart';

class ApiUri {
  // 字符转Uri
  Uri apiUri(String apiUri) {
    Uri uri = Uri.parse(ApiConfig.API + apiUri);
    return uri;
  }

  // 自定义域名
  Uri apiUriHost(String host, String apiUri) {
    Uri uri = Uri.parse(host + apiUri);
    return uri;
  }
}
