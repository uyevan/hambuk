// ignore: file_names
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';

class StatusBarColor extends StatelessWidget {
  final Color color;

  const StatusBarColor({Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: color,
      ),
      child: const SizedBox.shrink(),
    );
  }
}
