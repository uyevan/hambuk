class ApiConfig {
  static const String API = "https://kinohana.mukapat.com";
  static const String KEEPER = "3487297072";
  static const String APPID = "app001";
  static final ApiConfig _instance = ApiConfig._internal();

  factory ApiConfig() {
    return _instance;
  }

  ApiConfig._internal();

  /*用户令牌*/
  late String Token;

  void setToken(String token) {
    Token = token;
  }

  String getToken() {
    return Token;
  }

  /*应用通知*/
  late String notice;

  void setNotice(String Notice) {
    notice = Notice;
  }

  String getNotice() {
    return notice;
  }

  /*用户ID*/
  late String openID;

  void setOpenID(String OpenID) {
    openID = OpenID;
  }

  String getOpenID() {
    return openID;
  }
}
