/* 所有电影model */
class FilmModel {
  final int id;
  final String name;
  final String cover;
  final double rate;
  final String category_name;
  final String region_name;
  final int type;
  final int require_vip;
  final int total_episode_count;
  final int online_episode_count;

  FilmModel({
    required this.id,
    required this.name,
    required this.cover,
    required this.rate,
    required this.category_name,
    required this.region_name,
    required this.type,
    required this.require_vip,
    required this.total_episode_count,
    required this.online_episode_count,
  });

  factory FilmModel.fromJson(Map<String, dynamic> json) {
    return FilmModel(
      id: json['id'],
      name: json['name'],
      cover: json['cover'],
      rate: json['rate'].toDouble(),
      category_name: json['category_name'],
      region_name: json['region_name'],
      type: json['type'],
      require_vip: json['require_vip'],
      total_episode_count: json['total_episode_count'],
      online_episode_count: json['online_episode_count'],
    );
  }
}
