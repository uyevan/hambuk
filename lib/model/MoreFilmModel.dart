class EpisodeModel {
  final int id;
  final int vod_id;
  final int source_id;
  final int episode_num;
  final String url;
  final String? burl;
  final int free_seconds;
  final int require_vip;

  EpisodeModel({
    required this.id,
    required this.vod_id,
    required this.source_id,
    required this.episode_num,
    required this.url,
    required this.burl,
    required this.free_seconds,
    required this.require_vip,
  });

  factory EpisodeModel.fromJson(Map<String, dynamic> json) {
    return EpisodeModel(
      id: json['id'],
      vod_id: json['vod_id'],
      source_id: json['source_id'],
      episode_num: json['episode_num'],
      url: json['url'],
      burl: json['burl'],
      free_seconds: json['free_seconds'],
      require_vip: json['require_vip'],
    );
  }
}
