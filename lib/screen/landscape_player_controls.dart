/*Web模式需要开启*/
import 'dart:html';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:universal_platform/universal_platform.dart';

class LandscapePlayerControls extends StatelessWidget {
  const LandscapePlayerControls(
      {super.key, this.iconSize = 20, this.fontSize = 12});

  final double iconSize;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    /*全屏解决方案 --- Web模式需要开启*/
    late bool full = false;
    void full_() {
      if (full) {
        full = false;
        document.exitFullscreen();
      } else {
        full = true;
        document.documentElement?.requestFullscreen();
      }
    }

    return Stack(
      children: <Widget>[
        /*屏幕中*/
        Positioned.fill(
          child: FlickShowControlsAction(
            child: FlickSeekVideoAction(
              child: Center(
                child: FlickVideoBuffer(
                  child: FlickAutoHideChild(
                    showIfVideoNotInitialized: false,
                    child: FlickPlayToggle(
                      size: 24,
                      color: ColorTween(
                        begin: const Color(0xFFED1D24),
                        end: const Color(0xFFED1F69),
                      ).lerp(0.5),
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 0.5),
                        borderRadius: BorderRadius.circular(40),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        /*控制台*/
        Positioned.fill(
          child: FlickAutoHideChild(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  color: const Color.fromRGBO(0, 0, 0, 0.4),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      /*播放与暂停*/
                      const FlickPlayToggle(
                        size: 20,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      /*音量*/
                      const FlickSoundToggle(
                        size: 20,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      /*开始时间*/
                      FlickCurrentPosition(
                        fontSize: fontSize,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      /*进度条*/
                      Expanded(
                        child: FlickVideoProgressBar(
                          flickProgressBarSettings: FlickProgressBarSettings(
                            height: 5,
                            handleRadius: 8,
                            padding: const EdgeInsets.symmetric(
                              horizontal: 8.0,
                              vertical: 8,
                            ),
                            backgroundColor: Colors.white24,
                            bufferedColor: Colors.white38,
                            getPlayedPaint: (
                                {double? handleRadius,
                                double? height,
                                double? playedPart,
                                double? width}) {
                              return Paint()
                                ..shader = const LinearGradient(colors: [
                                  Color(0xFFED1D24),
                                  Color(0xFFED1F69),
                                ], stops: [
                                  0.0,
                                  0.5
                                ]).createShader(
                                  Rect.fromPoints(
                                    const Offset(0, 0),
                                    Offset(width!, 0),
                                  ),
                                );
                            },
                            getHandlePaint: (
                                {double? handleRadius,
                                double? height,
                                double? playedPart,
                                double? width}) {
                              return Paint()
                                ..shader = const RadialGradient(
                                  colors: [
                                    Color(0xFFED1D24),
                                    Color(0xFFED1F69),
                                    Colors.white,
                                  ],
                                  stops: [0.0, 0.5, 0.1],
                                  radius: 0.6,
                                ).createShader(
                                  Rect.fromCircle(
                                    center: Offset(playedPart!, height! / 2),
                                    radius: handleRadius!,
                                  ),
                                );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      /*结束时间*/
                      FlickTotalDuration(
                        fontSize: fontSize,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      /*全屏*/
                      kIsWeb
                          ? Column(
                              children: [
                                FlickFullScreenToggle(
                                  size: iconSize,
                                  /*Web模式需要开启*/
                                  toggleFullscreen:
                                      UniversalPlatform.isWeb ? full_ : null,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                              ],
                            )
                          : const Column(),
                      /*退出*/
                      GestureDetector(
                        onTap: () {
                          SystemChrome.setPreferredOrientations(
                              [DeviceOrientation.portraitUp]);
                          SystemChrome.setEnabledSystemUIMode(
                              SystemUiMode.manual,
                              overlays: SystemUiOverlay.values);
                          Navigator.pop(context);
                        },
                        child: const Opacity(
                          opacity: 1, // 设置透明度值
                          child: Icon(
                            Icons.cancel,
                            size: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
