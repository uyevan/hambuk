import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'landscape_player_controls.dart';

class VideoPlayerM extends StatefulWidget {
  final String url;
  final String icon;
  final String title;

  const VideoPlayerM(
      {super.key, required this.url, required this.icon, required this.title});

  @override
  State<VideoPlayerM> createState() => _MyVideoPlayerM();
}

class _MyVideoPlayerM extends State<VideoPlayerM> {
  late FlickManager flickManager;
  late String _videoUrl;


  /*创建界面事件*/
  @override
  void initState() {
    super.initState();
    _initializeVideoPlayer();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft]); // 横屏
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
        overlays: []); // 隐藏状态栏
    print(_videoUrl);
    flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.network(_videoUrl),
    );
  }

  /*销毁界面事件*/
  @override
  void dispose() {
    flickManager.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    super.dispose();
  }

  /*URL验证*/
  void _initializeVideoPlayer() async {
    _videoUrl = widget.url;
    /*if (_videoUrl.startsWith('http://')) {
      _videoUrl =  _videoUrl.replaceFirst('http://', 'https://');
    }*/
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        /*返回键事件*/
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        body: FlickVideoPlayer(
          flickManager: flickManager,
          preferredDeviceOrientation: const [
            DeviceOrientation.landscapeRight,
            DeviceOrientation.landscapeLeft,
          ],
          systemUIOverlay: const [],
          flickVideoWithControls: const FlickVideoWithControls(
            controls: LandscapePlayerControls(),
          ),
          flickVideoWithControlsFullscreen: const FlickVideoWithControls(
            controls: FlickLandscapeControls(),
          ),
        ),
      ),
    );
  }
}
